# uVPN
**Quickly set up OpenVPN with easy-rsa2 on Debian/Ubuntu family**

- Download/clone `build-ovpn`, `build-server` and `_vars` to the same directory:
	`git clone https://gitlab.com/pepa65/uvpn`
- Copy & rename `_vars` file and edit it (instructions in its comments)
- Run `sudo bash build-server <vars-file>`
  * Packages `openvpn` and `easy-rsa` will be installed if not present
  * Server-side certificates will be generated
  * OpenVPN server configuration will be (over)written [if confirmed]
  * The OpenVPN server will be (re)started [if confirmed]
- Run `sudo bash build-ovpn <vars-file>` to generate a OpenVPN client file
- Rerun `sudo bash build-ovpn <vars-file>` to generate more OpenVPN client
